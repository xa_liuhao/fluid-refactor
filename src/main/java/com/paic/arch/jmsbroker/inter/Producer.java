package com.paic.arch.jmsbroker.inter;

import com.paic.arch.jmsbroker.JmsMessageBrokerSupport;

/***
 * 
 * 为遵循SRP(单一责任原则)，将生产者相关业务逻辑抽取成一个单独接口
 * 
 * @author Liu
 *
 */
public interface Producer {
	JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend);
}
