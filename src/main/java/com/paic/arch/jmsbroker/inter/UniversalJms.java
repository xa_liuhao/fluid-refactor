package com.paic.arch.jmsbroker.inter;

import static org.slf4j.LoggerFactory.getLogger;

import org.slf4j.Logger;

//将常量抽取成一个公共接口
public interface UniversalJms {
	static final Logger LOG = getLogger(UniversalJms.class);

	static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";
}
