package com.paic.arch.jmsbroker.impl;

import javax.jms.MessageProducer;

import com.paic.arch.jmsbroker.JmsMessageBrokerSupport;
import com.paic.arch.jmsbroker.inter.Producer;

/***
 * 
 * 1、生产者相关业务逻辑整合到一个类中，继承了JmsMessageBrokerSupport公共抽象类
 * 2、实现了Producer接口
 * 
 * @author Liu
 *
 */
public class ProducerImpl extends JmsMessageBrokerSupport implements Producer {
	//构造器
	protected ProducerImpl(String aBrokerUrl) {
		super(aBrokerUrl);
	}

	public JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend) {
		executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
			MessageProducer producer = aSession.createProducer(aDestination);
			producer.send(aSession.createTextMessage(aMessageToSend));
			producer.close();
			return "";
		});
		return this;
	}

}
