package com.paic.arch.jmsbroker.inter;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

/***
 * 
 * 为使JmsCallback接口能公共访问，单独提取出来
 * 
 * @author Liu
 *
 */
public interface JmsCallback {
	String performJmsFunction(Session aSession, Destination aDestination) throws JMSException;
}
