package com.paic.arch.jmsbroker.inter;

/***
 * 
 * 
 * 为遵循SRP(单一责任原则)，将消费者相关业务逻辑抽取成一个单独接口
 * 
 * @author Liu
 *
 */
public interface Consumer {
	String retrieveASingleMessageFromTheDestination(String aDestinationName);
	
	String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout);
}
