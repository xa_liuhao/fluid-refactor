package com.paic.arch.jmsbroker.impl;

import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.TextMessage;

import com.paic.arch.jmsbroker.JmsMessageBrokerSupport;
import com.paic.arch.jmsbroker.inter.Consumer;

/***
 * 
 * 1、消费者相关业务逻辑整合到一个类中，继承了JmsMessageBrokerSupport公共抽象类
 * 2、实现了Consumer接口
 * 
 * @author Liu
 *
 */
public class ConsumerImpl extends JmsMessageBrokerSupport implements Consumer{
	//构造器
	protected ConsumerImpl(String aBrokerUrl) {
		super(aBrokerUrl);
	}

	private static final int ONE_SECOND = 1000;
	private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;

	public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
		return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
	}

	public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
		return executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
			MessageConsumer consumer = aSession.createConsumer(aDestination);
			Message message = consumer.receive(aTimeout);
			if (message == null) {
				throw new NoMessageReceivedException(
						String.format("No messages received from the broker within the %d timeout", aTimeout));
			}
			consumer.close();
			return ((TextMessage) message).getText();
		});
	}
}
